use bingus::rawdata::RawData;
use dasp_sample::U24;
use fundsp::math::uparc;
use image::{ImageReader, RgbImage};
use wasm_bindgen::{convert::RefFromWasmAbi, prelude::*};
#[allow(unused_macros)]
#[macro_use]
pub mod macros;
pub mod js;

#[wasm_bindgen]
pub fn greet(name: &str) {
    println!("Hello, {name}!");
}

#[wasm_bindgen(getter_with_clone)]
pub struct ImageFile {
    pub name: String,
    pub data: Vec<u8>,
}

#[wasm_bindgen]
pub fn process_files(files: Vec<ImageFile>) {
    files.into_iter().for_each(|file| {
        let img = RawData::<RgbImage>::new(
            ImageReader::open(file.name)
                .unwrap()
                .decode()
                .unwrap()
                .into_rgb8(),
        );
        img.par_pixels_mut().for_each(|px| {
            let sample = uparc(
                U24::new_unchecked((px[2] as i32) | ((px[1] as i32) << 8) | ((px[0] as i32) << 16))
                    .to_sample(),
            );
        });
        let processed = RawData::<RgbImage>::new(
            RgbImage::from_raw(
                img.width(),
                img.height(),
                img.par_chunks_exact(3)
                    .map(|px: &[u8]| {
                        (px[2] as i32) | ((px[1] as i32) << 8) | ((px[0] as i32) << 16)
                    })
                    .map(U24::new_unchecked)
                    .map(|x| uparc(x.to_sample()))
                    .flat_map(|sample: f32| {
                        let rgb: U24 = sample.to_sample();
                        let rgo = ((rgb) >> 8.into()) << 8.into();
                        let roo = ((rgo) >> 16.into()) << 16.into();
                        vec![roo, rgo, rgb]
                    })
                    .collect::<Vec<u8>>(),
            )
            .unwrap(),
        );
        let _ = processed;
    });
}
