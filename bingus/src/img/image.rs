use std::ops::{Deref, DerefMut};

pub use image::*;
use num::{
    traits::{FromBytes, ToBytes},
    Zero,
};
use rayon::iter::ParallelIterator;

use crate::{Bendable, IntoDataBytes, TryFromDataBytes};

impl<P: Pixel> IntoDataBytes for ImageBuffer<P, Vec<P::Subpixel>>
where
    Vec<P::Subpixel>: Deref<Target = [P::Subpixel]>,
    P::Subpixel: ToBytes,
{
    fn into_bytes(self) -> crate::Bytes {
        self.iter()
            .flat_map(|subpixel| subpixel.to_ne_bytes().as_ref().to_vec())
            .collect()
    }
}

pub struct Dimensions {
    pub width: u32,
    pub height: u32,
}

impl<P: Pixel> TryFromDataBytes for ImageBuffer<P, Vec<P::Subpixel>>
where
    Vec<P::Subpixel>: Deref<Target = [P::Subpixel]>,
    P::Subpixel: ToBytes + FromBytes,
    <P::Subpixel as FromBytes>::Bytes: for<'a> TryFrom<&'a [u8]>,
{
    type Error = ();
    type Format = Dimensions;
    fn try_from_bytes(
        bytes: crate::Bytes,
        format: Self::Format,
        crop: crate::Crop,
    ) -> Result<Self, Self::Error>
    where
        Self: Sized,
    {
        ImageBuffer::from_raw(
            format.width,
            format.height,
            match crop {
                crate::Crop::End => bytes
                    .chunks_exact(P::Subpixel::zero().to_ne_bytes().as_ref().len())
                    .map(|p| {
                        P::Subpixel::from_ne_bytes(
                            &match <P::Subpixel as FromBytes>::Bytes::try_from(p) {
                                Ok(v) => v,
                                Err(_) => unreachable!("you messed up chunk size!"),
                            },
                        )
                    })
                    .collect::<Vec<P::Subpixel>>(),
                crate::Crop::Start => bytes
                    .rchunks_exact(P::Subpixel::zero().to_ne_bytes().as_ref().len())
                    .map(|p| {
                        P::Subpixel::from_ne_bytes(
                            &match <P::Subpixel as FromBytes>::Bytes::try_from(p) {
                                Ok(v) => v,
                                Err(_) => unreachable!("you messed up chunk size!"),
                            },
                        )
                    })
                    .collect::<Vec<P::Subpixel>>(),
            },
        )
        .ok_or(())
    }
}

impl<P: Pixel> Bendable for ImageBuffer<P, Vec<P::Subpixel>>
where
    Vec<P::Subpixel>: Deref<Target = [P::Subpixel]> + DerefMut,
    P::Subpixel: ToBytes + FromBytes + Send + Sync,
    <P::Subpixel as FromBytes>::Bytes: for<'a> TryFrom<&'a [u8]>,
    P: Send + Sync,
{
    type Unit = P;
    fn map<F: Fn(&Self::Unit) -> Self::Unit + Sync>(mut self, f: F) -> Self {
        self.par_pixels_mut().for_each(|p| *p = f(p));
        self
    }
    fn format() -> crate::Format {
        crate::Format::Image
    }
}

impl IntoDataBytes for DynamicImage {
    fn into_bytes(self) -> crate::Bytes {
        self.into_bytes()
    }
}

#[cfg(test)]
mod tests {
    #[cfg(test)]
    mod ser_de {
        use super::super::{
            Dimensions, ImageBuffer, IntoDataBytes, Rgb, Rgb32FImage, RgbImage, RgbaImage,
            TryFromDataBytes,
        };

        #[test]
        fn empty() {
            let image = RgbImage::new(0, 0);
            assert_eq!(
                Ok(image.clone()),
                RgbImage::try_from_bytes(
                    image.into_bytes(),
                    Dimensions {
                        width: 0,
                        height: 0
                    },
                    Default::default()
                )
            )
        }

        #[test]
        fn simple() {
            let image = RgbImage::from_raw(3, 1, vec![1, 2, 3, 4, 5, 6, 7, 8, 9]).unwrap();
            assert_eq!(
                Ok(image.clone()),
                RgbImage::try_from_bytes(
                    image.into_bytes(),
                    Dimensions {
                        width: 3,
                        height: 1
                    },
                    Default::default()
                )
            )
        }

        #[test]
        fn rgba() {
            let image =
                RgbaImage::from_raw(3, 1, vec![1, 2, 3, 0, 4, 5, 6, 1, 7, 8, 9, 2]).unwrap();
            assert_eq!(
                Ok(image.clone()),
                RgbaImage::try_from_bytes(
                    image.into_bytes(),
                    Dimensions {
                        width: 3,
                        height: 1
                    },
                    Default::default()
                )
            )
        }

        #[test]
        fn rgb_u16() {
            let image = ImageBuffer::<Rgb<u16>, Vec<u16>>::from_raw(
                3,
                1,
                vec![1, 2, 3, 254, 255, 256, 307, 308, 309],
            )
            .unwrap();
            assert_eq!(
                Ok(image.clone()),
                ImageBuffer::<Rgb<u16>, Vec<u16>>::try_from_bytes(
                    image.into_bytes(),
                    Dimensions {
                        width: 3,
                        height: 1
                    },
                    Default::default()
                )
            )
        }

        #[test]
        fn rgb_signed() {
            let image = ImageBuffer::<Rgb<i16>, Vec<i16>>::from_raw(
                3,
                1,
                vec![1, 2, 3, 254, 255, 256, -307, 308, 309],
            )
            .unwrap();
            assert_eq!(
                Ok(image.clone()),
                ImageBuffer::<Rgb<i16>, Vec<i16>>::try_from_bytes(
                    image.into_bytes(),
                    Dimensions {
                        width: 3,
                        height: 1
                    },
                    Default::default()
                )
            )
        }

        #[test]
        fn rgb_f32() {
            let image = Rgb32FImage::from_raw(
                3,
                1,
                vec![1.0, 2.0, 3.0, 254.0, 255.0, 256.1, 307.0, 308.0, 309.0],
            )
            .unwrap();
            assert_eq!(
                Ok(image.clone()),
                Rgb32FImage::try_from_bytes(
                    image.into_bytes(),
                    Dimensions {
                        width: 3,
                        height: 1
                    },
                    Default::default()
                )
            )
        }
    }

    #[cfg(test)]
    mod effects {
        use crate::Bendable;

        use super::super::{Pixel, RgbImage};

        #[test]
        fn fill_with_funny_number() {
            let image = RgbImage::new(8, 16);
            let new_image = image.clone().map(|p| p.map(|_channel| 42u8));
            assert_ne!(image.clone(), new_image);
            assert_eq!(42, new_image.get_pixel(1, 2).channels()[1])
        }
    }
}
