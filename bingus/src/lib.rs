pub mod bin;
pub mod img;
pub mod snd;
pub mod txt;

pub(crate) type Bytes = Vec<u8>;

pub mod dynamic {
    use std::{
        fs::File,
        io::{self, Read},
        path::Path,
    };

    use super::{
        img::{self, DynamicImage},
        Bytes,
    };

    use infer::MatcherType;
    use strum::EnumDiscriminants;
    use thiserror::Error;

    #[derive(EnumDiscriminants)]
    #[strum_discriminants(name(Format))]
    pub enum DynamicBendable {
        Image(DynamicImage),
        Binary(Bytes),
        Sound,
        Text,
    }

    #[derive(Debug, Error)]
    pub enum OpenError {
        #[error("{0:?}")]
        Io(#[from] io::Error),
        #[error("{0:?}")]
        Image(#[from] img::ImageError),
    }

    pub fn open<P: AsRef<Path>>(path: P) -> Result<Option<DynamicBendable>, OpenError> {
        use MatcherType::*;
        infer::get_from_path(&path)?
            .map(|t| t.matcher_type())
            .map(|matcher| -> Result<Option<DynamicBendable>, OpenError> {
                Ok(match matcher {
                    Image => Some(DynamicBendable::Image(img::open(path)?)),
                    App | Archive => Some(DynamicBendable::Binary({
                        let mut buf = Vec::new();
                        File::open(path)?.read_to_end(&mut buf)?;
                        buf
                    })),
                    Audio => todo!(),
                    Book => todo!(),
                    Doc => todo!(),
                    Font => todo!(),
                    Text => todo!(),
                    Video => todo!(),
                    Custom => None,
                })
            })
            .transpose()
            .map(|opt| -> Option<DynamicBendable> { opt? })
    }
}

use dynamic::*;

pub trait Bendable: TryFromDataBytes + IntoDataBytes {
    type Unit;
    fn bend_into<T: Bendable>(
        self,
        format: <T as TryFromDataBytes>::Format,
        crop: Crop,
    ) -> Result<T, <T as TryFromDataBytes>::Error> {
        T::bend_from(self, format, crop)
    }
    fn bend_from<T: Bendable>(
        b: T,
        format: <Self as TryFromDataBytes>::Format,
        crop: Crop,
    ) -> Result<Self, <Self as TryFromDataBytes>::Error> {
        Self::try_from_bytes(b.into_bytes(), format, crop)
    }
    fn map<F: Fn(&Self::Unit) -> Self::Unit + Sync>(self, f: F) -> Self;
    fn format() -> Format;
}

pub trait IntoDataBytes: Sized {
    fn into_bytes(self) -> Bytes;
}

#[derive(Default)]
pub enum Crop {
    Start,
    #[default]
    End,
}

pub trait TryFromDataBytes {
    type Error;
    type Format;
    fn try_from_bytes(bytes: Bytes, format: Self::Format, crop: Crop) -> Result<Self, Self::Error>
    where
        Self: Sized;
}
