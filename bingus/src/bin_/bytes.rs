use std::convert::Infallible;

use rayon::iter::{IntoParallelRefMutIterator, ParallelIterator};

use crate::{Bendable, Bytes, IntoDataBytes, TryFromDataBytes};

impl IntoDataBytes for Bytes {
    fn into_bytes(self) -> Bytes {
        self
    }
}

impl TryFromDataBytes for Bytes {
    type Error = Infallible;
    type Format = ();
    fn try_from_bytes(bytes: Bytes, _: Self::Format, _: crate::Crop) -> Result<Self, Self::Error>
    where
        Self: Sized,
    {
        Ok(bytes)
    }
}

impl Bendable for Bytes {
    type Unit = u8;
    fn map<F: Fn(&Self::Unit) -> Self::Unit + Sync>(mut self, f: F) -> Self {
        self.par_iter_mut().for_each(|e| *e = f(e));
        self
    }
    fn format() -> crate::Format {
        crate::Format::Binary
    }
}
